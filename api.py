import flask
from flask import request, jsonify
from flask_cors import CORS
app = flask.Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

# Create some test data for our games catalog in the form of a list of dictionaries.
games = [
         {'id': 0,
         'title': 'Persona 5',
         'publisher': 'Atlus',
         'HLTB': '120 hours',
         'year_of_release': '2016'},
         {'id': 1,
         'title': 'Final Fantasy VI',
         'publisher': 'Square Soft',
         'HLTB': '40 hours',
         'year_of_release': '1994'},
         {'id': 2,
         'title': 'Red Dead Redemption',
         'publisher': 'Rockstar Games',
         'HLTB': '25 hours',
         'year_of_release': '2010'}
         ]




@app.route('/', methods=['GET'])
def home():
    return '''<h1>The video-game API</h1>
        <p>Designed as a part of a ramp-up.</p>'''


@app.route('/api/v1/resources/games/all', methods=['GET'])
def api_all():
    return jsonify(games)


@app.route('/api/v1/resources/games', methods=['GET'])
def api_id():
    if 'id' in request.args:
        id = int(request.args['id'])
    else:
        return "Error: No id field provided. Please specify an id."

    # Create an empty list for our results
    results = []


    for game in games:
        if game['id'] == id:
            results.append(game)
    return jsonify(results)



app.run()
